# About
This is the license that we use for some our projects. It's quite similar to the [WTFPL](http://www.wtfpl.net/).

```
                      SYYDAI FUCKING PUBLIC LICENSE
                        Version 0.1.3, 2024 April

Copyright (c) [year] <your name>

Everyone is permitted to copy, distribute, embed, modify, merge, publish,
redistribute, sell, sublicense, and/or whatever the fuck they want for any
purpose with or without fee. These files is offered as-is, without any warranty.

IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THESE FILES OR THE USE OR
OTHER DEALINGS IN THESE FILES. ALL WARRANTIES ARE HEREBY DISCLAIMED.
```